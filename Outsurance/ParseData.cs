﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic.FileIO;
using System.IO;


namespace Outsurance
{
    class ParseData
    {
        public System.Collections.ArrayList GetFirstName(string path)
        {
            System.Collections.ArrayList list = new System.Collections.ArrayList();
            var lines = File.ReadAllLines(path).Select(x => x.Split(','));
            
            int lineLength = lines.First().Count();
            var CSV = lines.Skip(1)
                       .SelectMany(x => x)
                       .Select((v, i) => new { Value = v, Index = i % lineLength })
                       .Where(x => x.Index == 0)
                        .OrderBy(x => x.Value)
                       .Select(x => x.Value)
                        .GroupBy(n => n);
            foreach (var data in CSV)
            {
                list.Add(new Results(data.Key, data.Count()));
               
            }
            return list;           
        }
        public System.Collections.ArrayList GetLastName(string path)
        {
            System.Collections.ArrayList list = new System.Collections.ArrayList();
            var lines = File.ReadAllLines(path).Select(x => x.Split(','));

            int lineLength = lines.First().Count();
            var CSV = lines.Skip(1)
                       .SelectMany(x => x)
                       .Select((v, i) => new { Value = v, Index = i % lineLength })
                       .Where(x => x.Index == 1)
                        .OrderBy(x => x.Value)
                       .Select(x => x.Value)
                        .GroupBy(n => n);
            foreach (var data in CSV)
            {
                list.Add(new Results(data.Key, data.Count()));

            }
            return list;
        }

        public System.Collections.ArrayList GetAddresses(string path)
        {
            System.Collections.ArrayList list = new System.Collections.ArrayList();
            var lines = File.ReadAllLines(path).Select(x => x.Split(','));

            int lineLength = lines.First().Count();
            var CSV = lines.Skip(1)
                       .SelectMany(x => x)
                       .Select((v, i) => new { Value = v, Index = i % lineLength })
                       .Where(x => x.Index == 2)
                        .OrderBy(x => x.Value).ToList()
                        .Select(x => x.Value)
                        .GroupBy(n => n);
            foreach (var data in CSV)
            {
                list.Add(new Results(data.Key, data.Count()));

            }
            return list;
        }

    }
}
public class Address
{
    public string FullAddress { get; set; }
    public int Number { get; set; }
    public string Street { get; set; }
}
