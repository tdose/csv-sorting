﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections;

namespace Outsurance
{
    public partial class CSV_Test : Form
    {
        public CSV_Test()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ParseData pd = new ParseData();
                string fileName = "EmployeeData.csv";
                string path = Path.Combine(Environment.CurrentDirectory, @"Data\", fileName);
                ArrayList listAddreses = pd.GetAddresses(path);
                ArrayList listFirstNames = pd.GetFirstName(path);
                ArrayList listLastNames = pd.GetLastName(path);
                dgvAddresses.DataSource = listAddreses;
                dgvFirstName.DataSource = listFirstNames;
                dgvLastNames.DataSource = listLastNames;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
