﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Outsurance
{
   public class Results
    {
        private string name;     
        private int frequency;
      
        public Results(string name, int frequency)
        {
            this.name = name;
            this.frequency =frequency;
         
        }
        /// <summary>
        ///  Name
        /// </summary>
        public string Name
        {
            get { return this.name; }
        }
        /// <summary>
        /// Frequency
        /// </summary>
        
        public int Frequency
        {
            get { return this.frequency; }
        }
        /// <summary>
        /// Skill set
        /// </summary>
       
    }
}
