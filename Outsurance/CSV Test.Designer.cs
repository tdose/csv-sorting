﻿namespace Outsurance
{
    partial class CSV_Test
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.dgvFirstName = new System.Windows.Forms.DataGridView();
            this.dgvLastNames = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvAddresses = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFirstName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLastNames)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAddresses)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(250, 25);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Run Test";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dgvFirstName
            // 
            this.dgvFirstName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFirstName.Location = new System.Drawing.Point(35, 95);
            this.dgvFirstName.Name = "dgvFirstName";
            this.dgvFirstName.Size = new System.Drawing.Size(240, 150);
            this.dgvFirstName.TabIndex = 2;
            // 
            // dgvLastNames
            // 
            this.dgvLastNames.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLastNames.Location = new System.Drawing.Point(306, 95);
            this.dgvLastNames.Name = "dgvLastNames";
            this.dgvLastNames.Size = new System.Drawing.Size(240, 150);
            this.dgvLastNames.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "First Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(303, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Last Name";
            // 
            // dgvAddresses
            // 
            this.dgvAddresses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAddresses.Location = new System.Drawing.Point(35, 279);
            this.dgvAddresses.Name = "dgvAddresses";
            this.dgvAddresses.Size = new System.Drawing.Size(508, 150);
            this.dgvAddresses.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 263);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Addresses";
            // 
            // CSV_Test
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(786, 499);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dgvAddresses);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgvLastNames);
            this.Controls.Add(this.dgvFirstName);
            this.Controls.Add(this.button1);
            this.Name = "CSV_Test";
            this.Text = "CSV_Test";
            ((System.ComponentModel.ISupportInitialize)(this.dgvFirstName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLastNames)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAddresses)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dgvFirstName;
        private System.Windows.Forms.DataGridView dgvLastNames;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvAddresses;
        private System.Windows.Forms.Label label3;
    }
}